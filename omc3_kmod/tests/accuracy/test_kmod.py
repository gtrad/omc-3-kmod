import shutil
from pathlib import Path
import pickle

import numpy as np
import pandas as pd
import pytest
import tfs

from omc3_kmod.definitions.constants import PLANES
from omc3_kmod.kmod.constants import BETA, ERR, AVERAGE, STAR, LSA_FILE_NAME, RESULTS_FILE_NAME, INSTRUMENT_NAMES, INSTRUMENT_LOCATIONS
from omc3_kmod.optics_measurements.constants import EXT
from omc3_kmod.run_kmod import analyse_kmod
from omc3_kmod.analyse_kmod_files import analyse_kmod_files

LIMITS = {"Accuracy": 1e-5, "Meas Accuracy": 0.01, "Num Precision": 1e-15, "Meas Precision": 0.1}


@pytest.mark.extended
def test_single_quad_average_beta(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'MQXA.1R1.B1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']

    to_app_dict = analyse_kmod(
        betastar=[0.25],
        waist=[0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23545.51962],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
    )
    
    beta_twiss = {"X": 2448.373188, "Y": 3091.5384176904704}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = to_app_dict['RQX.1R1'].headers[f"{AVERAGE}{BETA}{plane}"]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Accuracy"]
        beta_err_meas = to_app_dict['RQX.1R1'].headers[f"{ERR}{AVERAGE}{BETA}{plane}"]
        assert (np.abs(beta_err_meas)) < LIMITS["Num Precision"]


@pytest.mark.extended
def test_kmod_for_gui(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'MQXA.1R1.B1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQXA.1L1.B1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    to_app_dict = analyse_kmod(
        betastar=[0.25],
        waist=[0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[20020.3124],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[19968.0124],
                'circuitsK':-magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMSW.1L1.B1", "BPMSW.1R1.B1", "IP1"],
                     INSTRUMENT_LOCATIONS:[19972.5834, 20015.7264, 19994.1624]},
    )
    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    lsa_results = tfs.read(tmp_path / f'{LSA_FILE_NAME}{EXT}', index='NAME')
    beta_twiss = {"X": 0.25, "Y": 0.25}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Accuracy"]
        assert lsa_results.loc['IP1', f'{BETA}{plane}'] == beta_meas
        beta_err_meas = results[f"{ERR}{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_err_meas)) < LIMITS["Num Precision"]

    pd.testing.assert_frame_equal(to_app_dict['Results'], results)
    pd.testing.assert_frame_equal(to_app_dict['LSA_Results'].set_index('NAME'), lsa_results)


@pytest.mark.extended
def test_kmod_file_analysis(tmp_path, _kmod_inputs_path):
    path1=str(_kmod_inputs_path/'IP1_B1_reanalysis'/'MQXA1.R1.tfs')
    path2=str(_kmod_inputs_path/'IP1_B1_reanalysis'/'MQXA1.L1.tfs')
    magnet1_df = tfs.read(path1)
    magnet2_df = tfs.read(path2)
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['MAGNETNAMES']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['MAGNETNAMES']

    to_app_dict_direct = analyse_kmod(
        betastar=[11.00],
        waist=[0.0],
        circuits={
            "MQXA.1R1":{
                'MAGNETNAMES':['MQXA.1R1'],
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[1641.8210900000013],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "MQXA.1L1":{
                'MAGNETNAMES':['MQXA.1L1'],
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[1589.5210899999984],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMSW.1L1.B1", "BPMSW.1R1.B1", "IP1"],
                     INSTRUMENT_LOCATIONS:[1594.09209, 1637.23509, 1615.67109]},
    )
    to_app_dict_files = analyse_kmod_files(
        betastar=[11.00],
        waist=[0.0],
        files=[path1, path2],
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMSW.1L1.B1", "BPMSW.1R1.B1", "IP1"],
                     INSTRUMENT_LOCATIONS:[1594.09209, 1637.23509, 1615.67109]},
    )

    # drop time since analysis runs one after the other
    pd.testing.assert_frame_equal(to_app_dict_direct['Results'].drop('TIME', axis=1), to_app_dict_files['Results'].drop('TIME', axis=1))
    pd.testing.assert_frame_equal(to_app_dict_direct['LSA_Results'].set_index('NAME'), to_app_dict_files['LSA_Results'].set_index('NAME'))


@pytest.mark.extended
def test_kmod_withRWS_ip1b1(tmp_path, _kmod_inputs_path):
    magnet1_df = tfs.read(_kmod_inputs_path/'RWS_B1_plus'/'MQXA.1R1.B1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'RWS_B1_plus'/'MQXA.1L1.B1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    to_app_dict_plus=analyse_kmod(
        betastar=[0.25, 0.25],
        waist=[0.0, 0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23545.51962],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23493.21962],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
    )
    magnet1_df = tfs.read(_kmod_inputs_path/'RWS_B1_minus'/'MQXA.1R1.B1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'RWS_B1_minus'/'MQXA.1L1.B1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    to_app_dict_minus=analyse_kmod(
        betastar=[0.25, 0.25],
        waist=[0.0, 0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23545.51962],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23493.21962],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
    )

    for plane in ['WAISTX', 'WAISTY']:
        pd.testing.assert_series_equal(np.sign(to_app_dict_plus['Results'].loc[:,plane]),
                                    -1*np.sign(to_app_dict_minus['Results'].loc[:,plane]))

    
@pytest.mark.extended
def test_kmod_withRWS_ip1b2(tmp_path, _kmod_inputs_path):
    magnet1_df = tfs.read(_kmod_inputs_path/'RWS_B2_plus'/'MQXA.1R1.B2.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'RWS_B2_plus'/'MQXA.1L1.B2.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    to_app_dict_plus=analyse_kmod(
        betastar=[0.25, 0.25],
        waist=[0.0, 0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23545.51962],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23493.21962],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=2,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
    )
    magnet1_df = tfs.read(_kmod_inputs_path/'RWS_B2_minus'/'MQXA.1R1.B2.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'RWS_B2_minus'/'MQXA.1L1.B2.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    to_app_dict_minus=analyse_kmod(
        betastar=[0.25, 0.25],
        waist=[0.0, 0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23545.51962],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23493.21962],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=2,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
    )

    for plane in ['WAISTX', 'WAISTY']:
        pd.testing.assert_series_equal(np.sign(to_app_dict_plus['Results'].loc[:,plane]),
                                    -1*np.sign(to_app_dict_minus['Results'].loc[:,plane]))


@pytest.mark.extended
def test_kmod_measurement_ip1b1(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'IP1_B1_reanalysis'/'MQXA1.R1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'IP1_B1_reanalysis'/'MQXA1.L1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = 'MQXA1.R1'
    magnet2_df.headers['QUADRUPOLE'] = 'MQXA1.L1'

    to_app_dict = analyse_kmod(
        betastar=[11.],
        waist=[0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[1641.8210900000013],
                'circuitsK':magnet1_df['K'].to_list(),
                'qh1':magnet1_df['TUNEX'].to_list(),
                'err_qh1':magnet1_df['ERRTUNEX'].to_list(),
                'qv1':magnet1_df['TUNEY'].to_list(),
                'err_qv1':magnet1_df['ERRTUNEY'].to_list(),
                'qh2':magnet1_df['TUNEX'].to_list(),
                'err_qh2':magnet1_df['ERRTUNEX'].to_list(),
                'qv2':magnet1_df['TUNEY'].to_list(),
                'err_qv2':magnet1_df['ERRTUNEY'].to_list(),
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[1589.5210899999984],
                'circuitsK':magnet2_df['K'].to_list(),
                'qh1':magnet2_df['TUNEX'].to_list(),
                'err_qh1':magnet2_df['ERRTUNEX'].to_list(),
                'qv1':magnet2_df['TUNEY'].to_list(),
                'err_qv1':magnet2_df['ERRTUNEY'].to_list(),
                'qh2':magnet2_df['TUNEX'].to_list(),
                'err_qh2':magnet2_df['ERRTUNEX'].to_list(),
                'qv2':magnet2_df['TUNEY'].to_list(),
                'err_qv2':magnet2_df['ERRTUNEY'].to_list(),
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMSW.1L1.B1", "BPMSW.1R1.B1", "IP1"],
                     INSTRUMENT_LOCATIONS:[1594.09209, 1637.23509, 1615.67109]},
    )

    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    lsa_results = tfs.read(tmp_path / f'{LSA_FILE_NAME}{EXT}', index='NAME')
    beta_twiss = {"X": 10.70, "Y": 10.60}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Meas Accuracy"]
        assert lsa_results.loc['IP1', f'{BETA}{plane}'] == beta_meas


@pytest.mark.extended
def test_kmod_measurement_ip5b1(tmp_path, _kmod_inputs_path):

    d = pickle.load(open(_kmod_inputs_path/'IP5_B1'/'forMichaelIP5_incoherencyInLen.pk', 'rb'))
    d['outputdir']=tmp_path
    to_app_dict = analyse_kmod(
        d
    )

    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    lsa_results = tfs.read(tmp_path / f'{LSA_FILE_NAME}{EXT}', index='NAME')
    beta_twiss = {"X": 10.70, "Y": 10.70}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Meas Accuracy"]
        # in the pickle file, the IP5 marker is offset by 0.6m wrt to IP/center of insertion 
        # issue to be looked into, not the case for IP1 data
        # assert lsa_results.loc['IP5', f'{BETA}{plane}'] == beta_meas


@pytest.mark.extended
def test_kmod_measurement_ip1b2(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'IP1_B2_reanalysis'/'MQXA1.R1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'IP1_B2_reanalysis'/'MQXA1.L1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = 'MQXA1.R1'
    magnet2_df.headers['QUADRUPOLE'] = 'MQXA1.L1'

    to_app_dict = analyse_kmod(
        betastar=[11.],
        waist=[0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[1641.8943100000033],
                'circuitsK':magnet1_df['K'].to_list(),
                'qh1':magnet1_df['TUNEX'].to_list(),
                'err_qh1':magnet1_df['ERRTUNEX'].to_list(),
                'qv1':magnet1_df['TUNEY'].to_list(),
                'err_qv1':magnet1_df['ERRTUNEY'].to_list(),
                'qh2':magnet1_df['TUNEX'].to_list(),
                'err_qh2':magnet1_df['ERRTUNEX'].to_list(),
                'qv2':magnet1_df['TUNEY'].to_list(),
                'err_qv2':magnet1_df['ERRTUNEY'].to_list(),
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[1589.5943100000004],
                'circuitsK':magnet2_df['K'].to_list(),
                'qh1':magnet2_df['TUNEX'].to_list(),
                'err_qh1':magnet2_df['ERRTUNEX'].to_list(),
                'qv1':magnet2_df['TUNEY'].to_list(),
                'err_qv1':magnet2_df['ERRTUNEY'].to_list(),
                'qh2':magnet2_df['TUNEX'].to_list(),
                'err_qh2':magnet2_df['ERRTUNEX'].to_list(),
                'qv2':magnet2_df['TUNEY'].to_list(),
                'err_qv2':magnet2_df['ERRTUNEY'].to_list(),
                },
        },
        beam=2,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMSW.1L1.B2", "BPMSW.1R1.B2", "IP1"],
                     INSTRUMENT_LOCATIONS:[1594.16531, 1637.30831, 1615.74431]},
    )

    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    lsa_results = tfs.read(tmp_path / f'{LSA_FILE_NAME}{EXT}', index='NAME')
    beta_twiss = {"X": 11.00, "Y": 11.00}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Meas Accuracy"]
        assert lsa_results.loc['IP1', f'{BETA}{plane}'] == beta_meas


@pytest.mark.extended
def test_kmod_simulation_ip1b1(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'MQXA.1R1.B1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQXA.1L1.B1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    analyse_kmod(
        betastar=[0.25, 0.25],
        waist=[0.0, 0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23545.51962],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23493.21962],
                'circuitsK':-magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
    )
    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    beta_twiss = {"X": 0.25, "Y": 0.25}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Accuracy"]
        beta_err_meas = results[f"{ERR}{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_err_meas)) < LIMITS["Num Precision"]


@pytest.mark.extended
def test_kmod_simulation_ip1b2(tmp_path, _kmod_inputs_path):
    magnet1_df = tfs.read(_kmod_inputs_path/'MQXA.1R1.B2.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQXA.1L1.B2.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    analyse_kmod(
        betastar=[0.25, 0.25],
        waist=[0.0, 0.0],
        circuits={
            "RQX.1R1":{
                'MAGNETNAMES':'MQXA.1R1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23545.51962],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L1":{
                'MAGNETNAMES':'MQXA.1L1',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[23493.21962],
                'circuitsK':-magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=2,
        no_sig_digits=True,
        plots=False,
        no_autoclean=True,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
    )
    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    beta_twiss = {"X": 0.25, "Y": 0.25}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Accuracy"]
        beta_err_meas = results[f"{ERR}{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_err_meas)) < LIMITS["Num Precision"]


@pytest.mark.extended
def test_kmod_simulation_ip4b1_q5q6(tmp_path, _kmod_inputs_path):
    magnet1_df = tfs.read(_kmod_inputs_path/'MQY.5R4.B1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQY.6R4.B1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']
    analyse_kmod(
        betastar=[200.0],
        waist=[-100.0],
        circuits={
            "RQ5.R4B1":{
                'MAGNETNAMES':'MQY.5R4.B1',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3464.904216],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQ6.R4B1":{
                'MAGNETNAMES':'MQM.6R4.B1',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3501.904216],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.5e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMYB.5R4.B1", "BPLV.A6R4.B1", "BPMYA.6R4.B1"],
                     INSTRUMENT_LOCATIONS:[3467.622216, 3470.036216, 3499.186216]},
    )
    results = tfs.read(
        tmp_path / f"{LSA_FILE_NAME}{EXT}", index="NAME"
    )

    original = {
        "BPMYB.5R4.B1": (219.2303059, 392.2304271),
        "BPLV.A6R4.B1": (237.7129981, 364.7790992),
        "BPMYA.6R4.B1": (523.9728258, 114.3389766),
    }
    for inst in results.index:
        beta_x, beta_y = original[inst]
        betas = dict(X=beta_x, Y=beta_y)
        for plane in PLANES:
            beta_meas = results[f"{BETA}{plane}"].loc[inst]
            assert (np.abs(beta_meas - betas[plane])) / betas[plane] < LIMITS["Meas Accuracy"]
            beta_err_meas = results[f"{ERR}{BETA}{plane}"].loc[inst]
            assert (beta_err_meas / beta_meas) < LIMITS["Meas Precision"]


@pytest.mark.extended
def test_kmod_simulation_ip4b1_q6q7(tmp_path, _kmod_inputs_path):
    magnet1_df = tfs.read(_kmod_inputs_path/'MQY.6R4.B1.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQM.7R4.B1.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']
    analyse_kmod(
        betastar=[200.0],
        waist=[-100.0],
        circuits={
            "RQ6.R4B1":{
                'MAGNETNAMES':'MQY.6R4.B1',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3501.904216],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQ7.R4B1":{
                'MAGNETNAMES':'MQM.7R4.B1',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3598.277216],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.5e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMCS.7R4.B1", "BPM.7R4.B1", "BQSH.7R4.B1", "BPLH.7R4.B1"],
                     INSTRUMENT_LOCATIONS:[3595.510216, 3595.702216, 3507.186216, 3510.236216]},
    )
    results = tfs.read(
        tmp_path / f"{LSA_FILE_NAME}{EXT}", index="NAME"
    )

    original = {
        "BPMCS.7R4.B1": (36.42083322, 94.60412545),
        "BPM.7R4.B1": (36.13170676, 94.89455616),
        "BQSH.7R4.B1": (507.1213885, 90.71406114),
        "BPLH.7R4.B1": (479.6329751, 86.53317006),
    }
    for inst in results.index:
        beta_x, beta_y = original[inst]
        betas = dict(X=beta_x, Y=beta_y)
        for plane in PLANES:
            beta_meas = results[f"{BETA}{plane}"].loc[inst]
            assert (np.abs(beta_meas - betas[plane])) / betas[plane] < LIMITS["Meas Accuracy"]
            beta_err_meas = results[f"{ERR}{BETA}{plane}"].loc[inst]
            assert (beta_err_meas / beta_meas) < LIMITS["Meas Precision"]


@pytest.mark.extended
def test_kmod_simulation_ip4b2_q5q6(tmp_path, _kmod_inputs_path):
    magnet1_df = tfs.read(_kmod_inputs_path/'MQY.5R4.B2.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQY.6R4.B2.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']
    analyse_kmod(
        betastar=[200.0],
        waist=[-100.0],
        circuits={
            "RQ5.R4B2":{
                'MAGNETNAMES':'MQY.5R4.B2',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3465.056584],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQ6.R4B2":{
                'MAGNETNAMES':'MQM.6R4.B2',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3502.056584],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=2,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.5e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMYA.5R4.B2", "BPLH.6R4.B2", "BPMYB.6R4.B2"],
                     INSTRUMENT_LOCATIONS:[3467.774584, 3469.388584, 3499.338584]},
    )
    results = tfs.read(
        tmp_path / f"{LSA_FILE_NAME}{EXT}", index="NAME"
    )

    original = {
        "BPMYA.5R4.B2": (424.2878778, 267.3234773),
        "BPLH.6R4.B2": (404.4101313, 281.824353),
        "BPMYB.6R4.B2": (124.3603847, 623.8430119),
    }
    for inst in results.index:
        beta_x, beta_y = original[inst]
        betas = dict(X=beta_x, Y=beta_y)
        for plane in PLANES:
            beta_meas = results[f"{BETA}{plane}"].loc[inst]
            assert (np.abs(beta_meas - betas[plane])) / betas[plane] < LIMITS["Meas Accuracy"]
            beta_err_meas = results[f"{ERR}{BETA}{plane}"].loc[inst]
            assert (beta_err_meas / beta_meas) < LIMITS["Meas Precision"]


@pytest.mark.extended
@pytest.mark.xfail(reason='Relative accuracy not met, but beta_x-deviation < 10m')
def test_kmod_simulation_ip4b2_q6q7(tmp_path, _kmod_inputs_path):
    magnet1_df = tfs.read(_kmod_inputs_path/'MQY.6R4.B2.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQM.7R4.B2.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']
    analyse_kmod(
        betastar=[200.0],
        waist=[-100.0],
        circuits={
            "RQ6.R4B2":{
                'MAGNETNAMES':'MQY.6R4.B2',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3502.056584],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQ7.R4B2":{
                'MAGNETNAMES':'MQM.7R4.B2',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3598.429584],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=2,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.5e-5,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BQSV.7R4.B2", "BPLV.7R4.B2", "BPMCS.7R4.B2", "BPM.7R4.B2"],
                     INSTRUMENT_LOCATIONS:[3509.138584, 3511.188584, 3595.662584, 3595.854584]},
    )
    results = tfs.read(
        tmp_path / f"{LSA_FILE_NAME}{EXT}", index="NAME"
    )

    original = {
        "BQSV.7R4.B2": (98.02018826, 569.0243306),
        "BPLV.7R4.B2": (95.57795721, 543.5227177),
        "BPMCS.7R4.B2": (97.25544654, 14.21358964),
        "BPM.7R4.B2": (97.48681412, 14.17045722),
    }
    for inst in results.index:
        beta_x, beta_y = original[inst]
        betas = dict(X=beta_x, Y=beta_y)
        for plane in PLANES:
            beta_meas = results[f"{BETA}{plane}"].loc[inst] 
            assert (np.abs(beta_meas - betas[plane])) / betas[plane] < LIMITS["Meas Accuracy"]
            beta_err_meas = results[f"{ERR}{BETA}{plane}"].loc[inst]
            assert (beta_err_meas / beta_meas) < LIMITS["Meas Precision"]


@pytest.mark.extended
def test_kmod_phase_simulation_ip5b1(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'IP5_VdM_simulation'/'MQXA.1R5.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'IP5_VdM_simulation'/'MQXA.1L5.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    analyse_kmod(
        betastar=[19.2],
        waist=[0.0],
        circuits={
            "RQX.1R5":{
                'MAGNETNAMES':'MQXA.1R5',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[20020.3124],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L5":{
                'MAGNETNAMES':'MQXA.1L5',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[19968.0124],
                'circuitsK':-magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        phase_weight=0.5,
        phase_files=[_kmod_inputs_path/'twiss.dat', _kmod_inputs_path/'twiss.dat'],
        bpms=["BPMSW.1L5.B1" , "BPMSW.1R5.B1"],
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMSW.1L5.B1" , "BPMSW.1R5.B1", "IP1"],
                     INSTRUMENT_LOCATIONS:[19972.5834, 20015.7264, 19994.1624]},
    )

    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    lsa_results = tfs.read(tmp_path / f'{LSA_FILE_NAME}{EXT}', index='NAME')
    beta_twiss = {"X": 19.2, "Y": 19.2}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Accuracy"]
        beta_err_meas = results[f"{ERR}{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_err_meas)) < LIMITS["Num Precision"]


@pytest.mark.extended
def test_kmod_phase_measured_ip5b1(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'IP5_VdM_measurement'/'MQXA.1R5.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'IP5_VdM_measurement'/'MQXA.1L5.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    analyse_kmod(
        betastar=[19.2],
        waist=[0.0],
        circuits={
            "RQX.1R5":{
                'MAGNETNAMES':'MQXA.1R5',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[20020.3124],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L5":{
                'MAGNETNAMES':'MQXA.1L5',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[19968.0124],
                'circuitsK':-magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
        phase_weight=0.5,
        phase_files=[_kmod_inputs_path/'phase_x.tfs', _kmod_inputs_path/'phase_y.tfs'],
        bpms=["BPMSW.1L5.B1" , "BPMSW.1R5.B1"],
        instruments={INSTRUMENT_NAMES:["BPMSW.1L5.B1" , "BPMSW.1R5.B1", "IP1"],
                     INSTRUMENT_LOCATIONS:[19972.5834, 20015.7264, 19994.1624]},
    )
    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    lsa_results = tfs.read(tmp_path / f'{LSA_FILE_NAME}{EXT}', index='NAME')
    beta_twiss = {"X": 19.2, "Y": 19.2}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Accuracy"]
        beta_err_meas = results[f"{ERR}{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_err_meas)) < LIMITS["Num Precision"]


@pytest.mark.extended
def test_kmod_phase_measured_ip5b1_bbsrc(tmp_path, _kmod_inputs_path):

    magnet1_df = tfs.read(_kmod_inputs_path/'IP5_VdM_measurement'/'MQXA.1R5.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'IP5_VdM_measurement'/'MQXA.1L5.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']

    analyse_kmod(
        betastar=[19.2],
        waist=[0.0],
        circuits={
            "RQX.1R5":{
                'MAGNETNAMES':'MQXA.1R5',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[20020.3124],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQX.1L5":{
                'MAGNETNAMES':'MQXA.1L5',
                'MAGNETLENGTHS':[6.37],
                'MAGNETLOCATIONS':[19968.0124],
                'circuitsK':-magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=1,
        no_sig_digits=True,
        plots=False,
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.0e-5,
        outputdir=tmp_path,
        phase_weight=0.5,
        phase_files=[_kmod_inputs_path/'getphasex.tfs', _kmod_inputs_path/'getphasey.tfs'],
        bpms=["BPMSW.1L5.B1" , "BPMSW.1R5.B1"],
        instruments={INSTRUMENT_NAMES:["BPMSW.1L5.B1" , "BPMSW.1R5.B1", "IP1"],
                     INSTRUMENT_LOCATIONS:[19972.5834, 20015.7264, 19994.1624]},
    )
    results = tfs.read(tmp_path / f"{RESULTS_FILE_NAME}{EXT}")
    lsa_results = tfs.read(tmp_path / f'{LSA_FILE_NAME}{EXT}', index='NAME')
    beta_twiss = {"X": 19.2, "Y": 19.2}

    for plane in PLANES:
        beta_sim = beta_twiss[plane]
        beta_meas = results[f"{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_meas - beta_sim)) / beta_sim < LIMITS["Accuracy"]
        beta_err_meas = results[f"{ERR}{BETA}{STAR}{plane}"].loc[0]
        assert (np.abs(beta_err_meas)) < LIMITS["Num Precision"]


@pytest.mark.extended
@pytest.mark.skip
def test_kmod_outputdir_default(tmp_path, _kmod_inputs_path):
    """
    Copy input files to tmp_path, use it as input_source and assert the results go there when
    no outputdir is specified.
    """
    [shutil.copy(kmod_input, tmp_path) for kmod_input in _kmod_inputs_path.glob("*L4B2*")]
    analyse_kmod(
        betastar_and_waist=[200.0, -100.0],
        input_source=tmp_path,
        beam=2,
        no_sig_digits=True,
        plots=False,
        circuits=["RQ7.L4B2", "RQ6.L4B2"],
        cminus=0.0,
        misalignment=0.0,
        errorK=0.0,
        errorL=0.0,
        tune_uncertainty=0.5e-5,
    )
    assert (tmp_path / "MQM.7L4.B2-MQY.6L4.B2").exists()


@pytest.mark.extended
def test_kmod_error_default(tmp_path, _kmod_inputs_path):
    """
    Test that kmod uses default errors.
    """
    magnet1_df = tfs.read(_kmod_inputs_path/'MQY.5R4.B2.tfs')
    magnet2_df = tfs.read(_kmod_inputs_path/'MQY.6R4.B2.tfs')
    magnet1_df.headers['QUADRUPOLE'] = magnet1_df.headers['NAME']
    magnet2_df.headers['QUADRUPOLE'] = magnet2_df.headers['NAME']
    analyse_kmod(
        betastar=[200.0],
        waist=[-100.0],
        circuits={
            "RQ5.R4B2":{
                'MAGNETNAMES':'MQY.5R4.B2',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3465.056584],
                'circuitsK':magnet1_df['K'],
                'qh1':magnet1_df['TUNEX'],
                'err_qh1':magnet1_df['ERRTUNEX'],
                'qv1':magnet1_df['TUNEY'],
                'err_qv1':magnet1_df['ERRTUNEY'],
                'qh2':magnet1_df['TUNEX'],
                'err_qh2':magnet1_df['ERRTUNEX'],
                'qv2':magnet1_df['TUNEY'],
                'err_qv2':magnet1_df['ERRTUNEY'],
                },
            "RQ6.R4B2":{
                'MAGNETNAMES':'MQM.6R4.B2',
                'MAGNETLENGTHS':[3.4],
                'MAGNETLOCATIONS':[3502.056584],
                'circuitsK':magnet2_df['K'],
                'qh1':magnet2_df['TUNEX'],
                'err_qh1':magnet2_df['ERRTUNEX'],
                'qv1':magnet2_df['TUNEY'],
                'err_qv1':magnet2_df['ERRTUNEY'],
                'qh2':magnet2_df['TUNEX'],
                'err_qh2':magnet2_df['ERRTUNEX'],
                'qv2':magnet2_df['TUNEY'],
                'err_qv2':magnet2_df['ERRTUNEY'],
                },
        },
        beam=2,
        no_sig_digits=True,
        plots=False,
        outputdir=tmp_path,
        instruments={INSTRUMENT_NAMES:["BPMYA.5R4.B2", "BPLH.6R4.B2", "BPMYB.6R4.B2"],
                     INSTRUMENT_LOCATIONS:[3467.774584, 3469.388584, 3499.338584]},
    )
    results = tfs.read(
        tmp_path / f"{LSA_FILE_NAME}{EXT}", index="NAME"
    )

    original = {
        "BPMYA.5R4.B2": (424.2878778, 267.3234773),
        "BPLH.6R4.B2": (404.4101313, 281.824353),
        "BPMYB.6R4.B2": (124.3603847, 623.8430119),
    }
    for inst in results.index:
        beta_x, beta_y = original[inst]
        betas = dict(X=beta_x, Y=beta_y)
        for plane in PLANES:
            beta_meas = results[f"{BETA}{plane}"].loc[inst]
            assert (np.abs(beta_meas - betas[plane])) / betas[plane] < LIMITS["Meas Accuracy"]
            beta_err_meas = results[f"{ERR}{BETA}{plane}"].loc[inst]
            assert (beta_err_meas / beta_meas) < LIMITS["Meas Precision"]


@pytest.fixture()
def _kmod_inputs_path() -> Path:
    return Path(__file__).parent.parent / "inputs" / "kmod"
