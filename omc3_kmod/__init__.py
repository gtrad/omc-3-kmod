"""
The omc3 library
~~~~~~~~~~~~~~~~

omc3 is a tool package for the optics measurements and corrections group (OMC) at CERN.

:copyright: pyLHC/OMC-Team working group.
:license: GNU GPLv3, see the LICENSE file for details.
"""

__title__ = "omc3_kmod"
__description__ = "An accelerator physics tools package for the OMC team at CERN."
__url__ = "https://github.com/pylhc/omc3"
__version__ = "0.2.6"
__author__ = "pylhc"
__author_email__ = "pylhc@github.com"
__license__ = "GNU GPLv3"

__all__ = [__version__]
