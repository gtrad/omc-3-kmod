"""
Run Kmod
--------

Top-level script to analyse Kmod-results from the ``LHC`` and creating files for GUI and plotting
as well as returning beta-star and waist shift.
"""
from pathlib import Path
import itertools
import numpy as np
import pandas as pd
import tfs

from generic_parser import EntryPointParameters, entrypoint
from generic_parser.entry_datatypes import get_instance_faker_meta

from omc3_kmod.definitions.constants import PLANES
from omc3_kmod.kmod import analysis, helper
from omc3_kmod.kmod.constants import (BETA, ERR, EXT,
                                 LSA_FILE_NAME, RESULTS_FILE_NAME, STAR, CLEANED, K, TUNE, INSTRUMENT_NAMES, INSTRUMENT_LOCATIONS,
                                 HEADER_MAGNET_NAMES, HEADER_CIRCUIT, HEADER_MAGNET_LENGTHS, HEADER_MAGNET_LOCATIONS)
from omc3_kmod.utils import iotools, logging_tools

LOG = logging_tools.get_logger(__name__)

LSA_COLUMNS = ['NAME', f'{BETA}X', f'{ERR}{BETA}X', f'{BETA}Y', f'{ERR}{BETA}Y']

# replace method from generic parser here to enable working with numpy arrays
class DictAsString(metaclass=get_instance_faker_meta(str, dict)):
    """Use dicts in command line like {"key":value}."""
    def __new__(cls, s):
        if isinstance(s, dict):
            return s

        d = eval(s, {'array': np.array, 'nan': float('nan')})
        if not isinstance(d, dict):
            raise ValueError(f"'{s}' can't be converted to a dictionary.")
        return d


def kmod_params():
    parser = EntryPointParameters()
    parser.add_parameter(name='circuits',
                         type=DictAsString,
                         required=True,
                         help='Dictionary with key(-s) the modulated circuit and'\
                              'values a dict with keys magnet names, lengths, s-location, tunes, and gradient')
    parser.add_parameter(name='beam',
                         type=int,
                         choices=[1, 2], required=True,
                         help='define beam used: 1 or 2',)
    parser.add_parameter(name='betastar',
                         type=float,
                         nargs='+',
                         help='Estimated beta star of measurements',)
    parser.add_parameter(name='waist',
                         type=float,
                         nargs='+',
                         help='Estimated waist shift',)
    parser.add_parameter(name='cminus', 
                         type=float,                         
                         help='C Minus',)
    parser.add_parameter(name='misalignment',
                         type=float,
                         help='misalignment of the modulated quadrupoles in m',)
    parser.add_parameter(name='errorK',
                         type=float,
                         help='error in K of the modulated quadrupoles, relative to gradient',)
    parser.add_parameter(name='errorL',
                         type=float,                         
                         help='error in length of the modulated quadrupoles, unit m',)
    parser.add_parameter(name='tune_uncertainty',
                         type=float,
                         default=2.5e-5,
                         help='tune measurement uncertainty')
    parser.add_parameter(name='instruments',
                         type=DictAsString,
                         help='Dict containing name and s-location of instruments at which optics should be evaluated.'\
                              f'Example: {{"{INSTRUMENT_NAMES}":[BGI, BWS, IP], "{INSTRUMENT_LOCATIONS}":[1, 2, 3]}}'\
                              'Caveat: the user is responsible to check that the locations lie inbetween the two quadrupoles.',)
    parser.add_parameter(name='no_autoclean',
                         action='store_true',
                         help='flag for manually cleaning data')
    parser.add_parameter(name='no_sig_digits',
                         action='store_true',                         
                         help='flag to not use significant digits')
    parser.add_parameter(name='plots',
                         action='store_true',                         
                         help='flag to create any plots')
    parser.add_parameter(name='phase_files',
                         type=Path,
                         nargs=2,
                         help='Paths to the phase twiss files, either from model or measurement. One per plane, first file for X, second for Y.')
    parser.add_parameter(name='bpms',
                         type=str,
                         nargs=2,
                         help='BPMs to use for phase weight')
    parser.add_parameter(name='phase_weight',
                         type=float,
                         default=0.0,
                         help='weight in penalty function between phase and beta.'
                              'If weight=0 phase is not used as a constraint.')
    parser.add_parameter(name="outputdir",
                         type=Path,
                         help="Path where outputfiles will be stored, defaults "
                                                "to the given working_directory")

    return parser


@entrypoint(kmod_params(), strict=True)
def analyse_kmod(opt):

    LOG.info('Getting input parameter')
    opt = ensure_printable_opt(opt)
    iotools.save_config(opt.outputdir, opt, "omc3-kmod")

    magnet_dfs = {}
    for circuit, values in opt.circuits.items():
        magnet_dfs[circuit] = translate_to_tfs(circuit, values, opt.beam)

    opt = _check_opt(opt)
    output_dir = opt.outputdir
    iotools.create_dirs(output_dir)

    for circuit, magnet_df in magnet_dfs.items():
        tfs.write(output_dir /f'{circuit}{EXT}', magnet_df, headers_dict={key: str(value) for key, value in magnet_df.headers.items()})

    # clean data
    for circuit, magnet_df in magnet_dfs.items():
        LOG.info(f'Analysing circuit {circuit}')
        magnet_dfs[circuit] = helper.add_tune_uncertainty(magnet_df, opt.tune_uncertainty)
        magnet_dfs[circuit] = helper.clean_data(magnet_df, opt.no_autoclean)

    # add headers
    for circuit, magnet_df in magnet_dfs.items():
        LOG.info(f'Adding headers for {circuit}')
        magnet_dfs[circuit] = analysis.headers_for_df(magnet_df)

    # add average beta
    for circuit, magnet_df in magnet_dfs.items():
        LOG.info(f'Calculate average beta function for {circuit}')
        magnet_dfs[circuit] = analysis.get_av_beta(magnet_df)

    return_dict = {'Plot': create_plot_dict(magnet_dfs), **magnet_dfs}

    for circuit, magnet_df in magnet_dfs.items():
        tfs.write(output_dir /f'{circuit}{EXT}', magnet_df, headers_dict={key: str(value) for key, value in magnet_df.headers.items()})

    # if not 2 magnets, finish calculations, otherwise run simplex to get beta*
    if len(magnet_dfs)!=2:
        return return_dict

    if opt.plots:
        helper.plot_cleaned_data(list(magnet_dfs.values()), output_dir/'Fits.png', False)

    phase_adv, phase_error, bpm_ip_distance = get_phase_and_bpm_for_phase_weight(opt)

    opt.label = f'{"-".join([df.headers[HEADER_CIRCUIT] for df in magnet_dfs.values()])}'
    magnet_dfs, lstar, ip_position = analysis.get_lstar(magnet_dfs)
    LOG.info('Run simplex')
    results_df = analysis.analyse(magnet_dfs, opt, ip_position, bpm_ip_distance, phase_adv, phase_error)
    results_df = analysis.calc_betastar(opt, results_df, lstar)

    tfs.write(output_dir / f'{RESULTS_FILE_NAME}{EXT}', results_df)
    return_dict['Results'] = results_df

    if opt.instruments is not None:
        lsa_df = analysis.calc_beta_at_instruments(opt, results_df)
        tfs.write(output_dir / f'{LSA_FILE_NAME}{EXT}', lsa_df)

        return_dict['LSA_Results'] = lsa_df

    return return_dict


def create_plot_dict(magnet_dfs):
    return [
            {
                'title':magnet_df.headers[HEADER_MAGNET_NAMES],
                'x_label':r'$ \Delta K$',
                'y_label':f'$Q_{plane}$',
                'curves':[
                    {
                        'label':'raw data',
                        'X': helper.return_delta_k(magnet_df),
                        'Y': helper.return_Q(magnet_df, plane),
                        f'{ERR}Y': helper.return_err_Q(magnet_df, plane),
                        'color': 'blue'
                    },
                    {
                        'label':'cleaned data',
                        'X': helper.return_delta_k(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == True, :]),
                        'Y': helper.return_Q(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == True, :], plane),
                        f'{ERR}Y': helper.return_err_Q(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == True, :], plane),
                        'color': 'orange'
                    },
                    {
                        'label':'fit',
                        'X': helper.return_delta_k(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == True, :]),
                        'Y': helper.return_fit_data(magnet_df.loc[magnet_df.loc[:, f"{CLEANED}{plane}"].values == True, :], plane),
                        'color': 'red'
                    }
                ],
            }
            for magnet_df, plane in itertools.product([df for df in magnet_dfs.values()], PLANES)
        ]


def _check_opt(opt):

    opt.betastar = convert_input_to_dict(opt.betastar)
    opt.waist = convert_input_to_dict(opt.waist)
    for error in ("cminus", "errorK", "errorL", "misalignment"):
        opt = check_default_error(opt, error)

    if opt.outputdir is None:
        opt.outputdir = Path("./")
    if opt.instruments is not None:
        opt.instruments = tfs.TfsDataFrame(opt.instruments)

    return opt


def check_phase_options(opt):

    if opt.phase_files is None and opt.bpms is None:
        return False
    if opt.phase_files is not None and len(opt.phase_files) == 2 and opt.bpms is None:
        raise AttributeError("BPMs need to be set.")
    if len(opt.bpms) == 2 and opt.phase_files is None:
        raise AttributeError("Phasefiles need to be set.")
    if len(opt.bpms) == 2 and len(opt.phase_files) == 2 and opt.phase_weight == 0.0:
        raise AttributeError("Phasefile and BPMs provided, but phaseweight set to 0.")
    return True


def check_bpm_in_instruments(bpms, instruments):
    if bpms is not None and instruments is not None:
        for bpm in bpms:
            if bpm not in instruments:
                LOG.warn(f'{bpm} not found in instruments, double check if correct BPM is given.' )

def get_phase_and_bpm_for_phase_weight(opt):

    check_bpm_in_instruments(opt.bpms, opt.instruments)
    phase_adv = {plane: 0.0 for plane in PLANES}
    phase_error = {plane: 0.0 for plane in PLANES}
    bpm_ip_distance = 0.0
    if check_phase_options(opt):
        for i, plane in enumerate(PLANES):
            twiss_df = tfs.read(opt.phase_files[i])
            # omc3 style
            if 'NAME' in twiss_df.columns and 'NAME2' in twiss_df.columns and 'S' in twiss_df.columns and 'S2' in twiss_df.columns:
                twiss_df.set_index('NAME', inplace=True)
                measurement_line = twiss_df.loc[opt.bpms[0]]
                if measurement_line.NAME2 != opt.bpms[1]:
                    measurement_line = twiss_df.loc[opt.bpms[1]]
                    assert measurement_line.NAME2 == opt.bpms[0]

                bpm_ip_distance= 0.5*np.abs(measurement_line.loc['S'] - measurement_line.loc['S2'])

                phase_adv[plane] = measurement_line.loc[f'PHASE{plane}']
                phase_error[plane] = measurement_line.loc[f'ERRPHASE{plane}']

            # getllm style
            elif 'NAME' in twiss_df.columns and 'NAME2' in twiss_df.columns and 'S' in twiss_df.columns and 'S1' in twiss_df.columns:
                twiss_df.set_index('NAME', inplace=True)
                measurement_line = twiss_df.loc[opt.bpms[0]]
                if measurement_line.NAME2 != opt.bpms[1]:
                    measurement_line = twiss_df.loc[opt.bpms[1]]
                    assert measurement_line.NAME2 == opt.bpms[0]

                bpm_ip_distance= 0.5*np.abs(measurement_line.loc['S'] - measurement_line.loc['S1'])

                phase_adv[plane] = measurement_line.loc[f'PHASE{plane}']
                phase_error[plane] = measurement_line.loc[f'STDPH{plane}']

            # model twiss style
            else:
                twiss_df.set_index('NAME', inplace=True)
                bpm_ip_distance= 0.5*np.abs(twiss_df.loc[opt.bpms[0], 'S'] - twiss_df.loc[opt.bpms[1], 'S'])
                phase_adv[plane] = np.abs(twiss_df.loc[opt.bpms[0], f'MU{plane}'] - twiss_df.loc[opt.bpms[1], f'MU{plane}'])
                phase_error[plane] = 0.5e-3  # this number is given by Andreas' estimations\

    return phase_adv, phase_error, bpm_ip_distance


def ensure_printable_opt(opt):

    for circuit in opt.circuits.keys():
        for column in ['MAGNETNAMES', 'MAGNETLENGTHS', 'MAGNETLOCATIONS', 'circuitsK', 'qh1', 'err_qh1', 'qv1', 'err_qv1', 'qh2', 'err_qh2', 'qv2', 'err_qv2']:
            try:
                opt.circuits[circuit][column] = list(opt.circuits[circuit][column])
            except KeyError:
                pass

    return opt


def translate_to_tfs(circuit, data, beam):

    header = {key: data[key] for key in [HEADER_MAGNET_NAMES, HEADER_MAGNET_LENGTHS, HEADER_MAGNET_LOCATIONS] }
    header[HEADER_CIRCUIT] = circuit


    # TODO at the moment, K for triplet magnets have the same sign for both beams
    # this is flipped here
    # compatibility with non-common magnets to be tested
    sign=1
    if 'QX' in circuit and beam ==2:
        sign=-1
    return tfs.TfsDataFrame({
        K:sign*np.array(data['circuitsK']),
        f'{TUNE}X':data[f'qh{beam}'],
        f'{ERR}{TUNE}X':data[f'err_qh{beam}'],
        f'{TUNE}Y':data[f'qv{beam}'],
        f'{ERR}{TUNE}Y':data[f'err_qv{beam}'],
     }, headers=header).dropna()


def convert_input_to_dict(bs):
    if len(bs) == 1:
        return dict(X=bs[0], Y=bs[0])
    elif len(bs) == 2:
        return dict(X=bs[0], Y=bs[1])
    else:
        raise ValueError('More than two entries provided for one option.')


def check_default_error(options, error):
    if options[error] is None:
        options[error] = DEFAULTS_CIRCUITS[error]
    return options


DEFAULTS_IP = {
    "cminus": 1E-3,
    "misalignment": 0.006,
    "errorK": 0.001,
    "errorL": 0.001,
}

DEFAULTS_CIRCUITS = {
    "cminus": 1E-3,
    "misalignment": 0.001,
    "errorK": 0.001,
    "errorL": 0.001,
}


if __name__ == '__main__':
    analyse_kmod()
